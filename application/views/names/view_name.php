<h1 class="text-primary text-center"><?php echo $name; ?></h1>

 <?php foreach ($blog as $blog_items){ ?>

<div class="card border-primary mb-3 text-center mx-auto" style="max-width: 100rem;">
  <div class="card-header"><h3 class="card-title"><?php echo $blog_items->title; ?></h3></div>
  <div class="card-body">
    <p class="card-text"><?php echo $blog_items->post; ?></p>
  </div>
</div>

	<?php } ?>