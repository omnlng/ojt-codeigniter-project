<?php

	public function view($page = 'home')
	{
		if (! file_exixts(APPPATH.'views/pages/'.$page.'.php'))
		 {
			show_404();
		}

		$data['title'] = ucfirst($page);

		$this->load->view('templates/header', $data);
		$this->load->view('pages/'.$page, $data);
		$this->load->view('templates/footer', $data);
	}

?>