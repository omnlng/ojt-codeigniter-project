<h1 class="text-primary text-center">Register</h1>

<?php echo form_open('users/register'); ?>

<div class="card border-primary mb-3 mx-auto" style="max-width: 50rem;">
  <div class="card-body">
    <div class="form-group">
      <label for="name">Name</label>
      <input type="input" class="form-control" name="name" id="name">
      <?php echo form_error('name', '<div class="text-danger">', '</div>'); ?>
    </div>

    <div class="form-group">
      <label for="email">Email</label>
      <input type="email" class="form-control" id="email" name="email">
      <?php echo form_error('email', '<div class="text-danger">', '</div>'); ?>
    </div>

    <div class="form-group">
      <label for="username">Username</label>
      <input type="input" class="form-control" id="username" name="username">
      <?php echo form_error('username', '<div class="text-danger">', '</div>'); ?>
    </div>

    <div class="form-group">
      <label for="password">Password</label>
      <input type="password" class="form-control" id="password" name="password">
      <?php echo form_error('password', '<div class="text-danger">', '</div>'); ?>
    </div>

    <div class="form-group">
      <label for="password2">Confirm Password</label>
      <input type="password" class="form-control" id="password2" name="password2">
      <?php echo form_error('password2', '<div class="text-danger">', '</div>'); ?>
    </div>
   
	<input type="submit" name="submit" class="btn btn-primary" value="Submit"/>
  </div>

    <p class="text-center">Already have an account? <a href="<?php echo site_url(); ?>users/login">Login Here</a></p>
</div>
<?php echo form_close(); ?>