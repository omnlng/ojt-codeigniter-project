<h1 class="text-primary text-center">Login</h1>

<?php echo form_open('users/login'); ?>

<div class="card border-primary mb-3 mx-auto" style="max-width: 50rem;">
  <div class="card-body">

    <div class="form-group">
      <label for="username">Username</label>
      <input type="text" class="form-control" id="username" name="username">
      <?php echo form_error('username', '<div class="text-danger">', '</div>'); ?>
    </div>

    <div class="form-group">
      <label for="password">Password</label>
      <input type="password" class="form-control" id="password" name="password">
      <?php echo form_error('password', '<div class="text-danger">', '</div>'); ?>
    </div>
   
	<input type="submit" name="submit" class="btn btn-primary" value="Login"/>
  </div>

  <p class="text-center">No account yet? <a href="<?php echo site_url(); ?>users/register">Register Here</a></p>
</div>
<?php echo form_close(); ?>