<h1 class="text-primary text-center">Home</h1>

<?php foreach ($blog as $blog_item): ?>

<div class="card border-primary mb-3 text-center mx-auto" style="max-width: 100rem;">
  <div class="card-header"><h3><a href="<?php echo site_url('blogs/'.$blog_item['slug']);?>" class="card-title"><?php echo $blog_item['title']; ?></a></h3></div>
  <div class="card-body">
    <p class="card-text"><?php echo word_limiter($blog_item['post'], 50); ?></p>
    <h6 class="text-secondary text-right"><a href="<?php echo site_url('names/'.$blog_item['name']);?>">-<?php echo $blog_item['name']; ?></a></h6>
  </div>
</div>
	
	<?php endforeach; ?>