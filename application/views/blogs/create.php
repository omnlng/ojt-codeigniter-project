

<h1 class="text-primary text-center">Create Post</h1>


<?php echo form_open('blogs/create'); ?>


<div class="card border-primary mb-3 mx-auto" style="max-width: 50rem;">
  <div class="card-body">
    <div class="form-group">
      <label for="name">Name</label>
      <input type="input" class="form-control" name="name" id="name" placeholder="Enter name">
      <?php echo form_error('name', '<div class="text-danger">', '</div>'); ?>
    </div>

    <div class="form-group">
      <label for="age">Age</label>
      <input type="input" class="form-control" name="age" id="age" placeholder="Enter age">
      <?php echo form_error('age', '<div class="text-danger">', '</div>'); ?>
    </div>

	<label for="gender">Gender</label>
    <div class="form-check">
        <label class="form-check-label">
          <input type="radio" class="form-check-input" name="gender" id="gender" value="Male" checked>
          Male
        </label>

    </div>
    <div class="form-check">
        <label class="form-check-label">
          <input type="radio" class="form-check-input" name="gender" id="gender" value="Female">
          Female
        </label>
        <?php echo form_error('gender', '<div class="text-danger">', '</div>'); ?>
    </div>
    <br>

    <div class="form-group">
      <label for="title">Title</label>
      <input type="input" class="form-control" id="title" name="title" placeholder="Enter title">
      <?php echo form_error('title', '<div class="text-danger">', '</div>'); ?>
    </div>

    <div class="form-group">
      <label for="post">Content</label>
      <textarea class="form-control" id="post" name="post" rows="3"></textarea>
      <?php echo form_error('post', '<div class="text-danger">', '</div>'); ?>
    </div>
   
	<input type="submit" name="submit" class="btn btn-primary" value="Post"/>
  </div>
</div>
