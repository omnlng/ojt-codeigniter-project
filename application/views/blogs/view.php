<h1 class="text-primary text-center"><?php echo $blog_item['title']; ?></h1>

<div class="card border-primary mb-3 text-center mx-auto" style="max-width: 100rem;">
  <div class="card-body">
    <p class="card-text"><?php echo $blog_item['post']; ?></p>
    <h6 class="text-secondary text-right">-<?php echo $blog_item['name']; ?></h6>
  </div>
</div>