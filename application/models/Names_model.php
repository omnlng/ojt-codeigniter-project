<?php

	class Names_model extends CI_Model
	{
		public function __construct()
		{
			$this->load->database();
		}

		public function get_posts($name)
		{

			$this->db->select('*');
			$this->db->where('name', $name);
			$query = $this->db->get('blog');
			return $query->result();

		}

	}

?>