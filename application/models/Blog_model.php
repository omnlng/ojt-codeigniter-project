<?php

	class Blog_model extends CI_Model
	{
		
		public function __construct()
		{
			$this->load->database();
		}

		public function get_blog($slug = FALSE)
		{
			if($slug === FALSE)
			{
				$query = $this->db->get('blog');
				return $query->result_array();
			}

			$query = $this->db->get_where('blog
				', array('slug' => $slug));
			return $query->row_array();
		}

		public function set_blog()
		{
			$this->load->helper('url');

			$slug = url_title($this->input->post('title'), 'dash', TRUE);
			$name = url_title($this->input->post('name'));

			$data = array(
						'name' 		=> $this->input->post('name'),
						'age'  		=> $this->input->post('age'),
						'gender' 	=> $this->input->post('gender'),
						'title'		=> $this->input->post('title'),
						'post'		=> $this->input->post('post'),
						'slug'		=> $slug
			);

			return $this->db->insert('blog', $data);
		}

		


	}

?>