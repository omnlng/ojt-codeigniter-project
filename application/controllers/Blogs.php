
<?php
	
	class Blogs extends CI_Controller
	{
		
		public function __construct()
		{
			parent::__construct();
			$this->load->model('blog_model');
			$this->load->helper('url_helper');
		}

		public function index()
		{
			$data['blog'] = $this->blog_model->get_blog();
			$data['title'] = 'Title';

			$this->load->view('templates/header', $data);
			$this->load->view('blogs/index', $data);
			$this->load->view('templates/footer');
		}

		public function view($slug = NULL)
		{
			$data['blog_item'] = $this->blog_model->get_blog($slug);

			if(empty($data['blog_item']))
			{
				show_404();
			}

			$data['title'] = $data['blog_item']['title'];

			$this->load->view('templates/header', $data);
			$this->load->view('blogs/view', $data);
			$this->load->view('templates/footer');
		}


		public function create()
		{
			$this->load->helper('form');
			$this->load->library('form_validation');

			$data['title'] = 'Post';

			$this->form_validation->set_rules('name', 'Name', 'required');
			$this->form_validation->set_rules('age', 'Age', 'required');
			$this->form_validation->set_rules('gender', 'Gender', 'required');
			$this->form_validation->set_rules('title', 'Title', 'required|is_unique[blog.title]');
			$this->form_validation->set_rules('post', 'Post', 'required');
		

		if($this->form_validation->run() === FALSE)
		{
			$this->load->view('templates/header', $data);
			$this->load->view('blogs/create');
			$this->load->view('templates/footer');
		}
		else
		{
			$this->blog_model->set_blog();
			$this->load->view('blogs/success');
		}


		}
	}


?>